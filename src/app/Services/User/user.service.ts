import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from 'src/app/Models/UserModel';
import { Observable } from 'rxjs';
 
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  
  constructor(private varHttp:HttpClient) { }

  GetAllUserDetails() : Observable<UserModel[]>
  { 
    return this.varHttp.get<UserModel[]>("http://localhost/Web.API/GetAllUserDetails");
 }
 AddUserDetails(value:UserModel)  
 { 
  return this.varHttp.post<UserModel>("http://localhost/Web.API/AddUserDetails" , value, httpOptions);
 }
 ModifyUserDetails(value:UserModel)
 { 
  return this.varHttp.put<UserModel>("http://localhost/Web.API/UpdateUserDetails" , value, httpOptions);  
 } 
 DeleteUserDetails(value:any)
 {
 return this.varHttp.delete("http://localhost/Web.API/DeleteUserDetails?userID="+ value);

 }
 UpdateUserOnEditTask(value:UserModel)
 { 
  return this.varHttp.put<UserModel>("http://localhost/Web.API/UpdateUserOnEditTask" , value, httpOptions);  
 } 
 
 GetSearchUserIDByTaskID(value:any) : Observable<UserModel>
 { 
  return this.varHttp.get<UserModel>("http://localhost/Web.API/GetSearchUserIDByTaskId?taskID="+ value);
 }
  


}
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'    
  })
}

