import { Injectable } from '@angular/core'; 
import { HttpClient } from '@angular/common/http';
import { TaskModel } from 'src/app/Models/TaskModel';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { ParentTaskModel } from 'src/app/Models/ParentTaskModel';



@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private varHttp:HttpClient) { }


  getTask() : Observable<TaskModel>
   { 
     return this.varHttp.get<TaskModel>("http://localhost/Web.API/GetTaskID");
  }
  getTaskName(value:any) : Observable<TaskModel>
  {     
    return this.varHttp.get<TaskModel>("http://localhost/Web.API/GetSearchTaskID?TaskID="+ value);
 } 

 GetTasksByProjectID(value:any) : Observable<TaskModel>
 { 
  return this.varHttp.get<TaskModel>("http://localhost/Web.API/GetTasksByProjectID?projectID="+ value);
}
GetSearchTaskID(value:any) : Observable<TaskModel>
{     
  return this.varHttp.get<TaskModel>("http://localhost/Web.API/GetSearchTaskID?TaskID="+ value);
} 

 AddTaskDetails(value:TaskModel)  
 {
  return this.varHttp.post<TaskModel>("http://localhost/Web.API/AddTaskDetails" , value, httpOptions);
 }
 ModifyTaskDetails(value:TaskModel)
 {
  return this.varHttp.put<TaskModel>("http://localhost/Web.API/UpdateTaskDetails" , value, httpOptions);

 }

 DeleteTaskDetails(value:any)
 {
 return this.varHttp.delete("http://localhost/Web.API/DeleteTaskDetails?taskID="+ value);
 }

 GetAllTaskDetails() : Observable<TaskModel[]>
 { 
   return this.varHttp.get<TaskModel[]>("http://localhost/Web.API/GetAllTaskDetails");
}
GetAllParentTasks() : Observable<ParentTaskModel[]>
{
  return this.varHttp.get<ParentTaskModel[]>("http://localhost/Web.API/GetAllParentTasks");
}
AddParentTask(value : ParentTaskModel) 
{
 
  return this.varHttp.post<ParentTaskModel>("http://localhost/Web.API/AddParentTasksDetail", value, httpOptions);
} 
GetParentName(value:any) 
{ 
 return this.varHttp.get<ParentTaskModel>("http://localhost/Web.API/GetParentName?parentID="+ value);
}
 

}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'    
  })
}
