import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { ProjectModel,ProjectTaskDetails } from 'src/app/Models/ProjectModel';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private varHttp:HttpClient) { }

  // getProjects():Observable<ProjectTaskDetails[]>{    
  //   return this.varHttp.get<ProjectTaskDetails[]>("http://localhost/Web.API/GetAllProjectDetails");
  //   //  .pipe(map((res:Response)=> res.json()));
  // }
  GetAllProjectTaskDetails(): Observable<ProjectTaskDetails[]>{    
    return this.varHttp.get<ProjectTaskDetails[]>("http://localhost/Web.API/GetAllProjectTaskDetails");    
  }

  GetAllProjectDetails() : Observable<ProjectModel[]>
  { 
    return this.varHttp.get<ProjectModel[]>("http://localhost/Web.API/GetAllProjectDetails");
 }
 AddProjectDetails(value:ProjectModel)  
 { 
  return this.varHttp.post<ProjectModel>("http://localhost/Web.API/AddProjectDetails" , value, httpOptions);
 }
 DeleteProjectDetails(value : any)
 {
  return this.varHttp.delete("http://localhost/Web.API/DeleteProjectDetails?projectID="+ value);
 }
 UpdateProjectDetails(value:ProjectModel)  
 { 
  return this.varHttp.put<ProjectModel>("http://localhost/Web.API/UpdateProjectDetails" , value, httpOptions);  
 }
 GetTasksByProjectID(value:any) : Observable<ProjectModel>
 { 
  debugger;
  return this.varHttp.get<ProjectModel>("http://localhost/Web.API/GetSearchProjectID?ProjectID="+ value);
}
GetSearchProjectID(value:any) : Observable<ProjectModel>
{ 
 return this.varHttp.get<ProjectModel>("http://localhost/Web.API/GetSearchProjectID?ProjectID="+ value);
}
 
}
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'    
  })
}


 