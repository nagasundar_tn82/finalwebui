import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Input } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { Output } from '@angular/core';
import { ProjectModel } from 'src/app/models/ProjectModel';
import { UserModel } from 'src/app/models/UserModel';
import { UserService } from 'src/app/Services/User/user.service';
import { TaskService } from 'src/app/Services/Task/task.service';
import {ProjectService} from  'src/app/Services/Project/project.service';
import { EventEmitter } from '@angular/core';
import { ParentTaskModel } from 'src/app/models/ParentTaskModel';


@Component({
  selector: 'app-popup-model',
  templateUrl: './popup-model.component.html',
  styleUrls: ['./popup-model.component.css']
})
export class PopupModelComponent implements OnInit {
  @Input() searchType: string;
  @Output() searchResult = new EventEmitter<any>();
  allTask : ParentTaskModel[];
  allProject : ProjectModel[];
  allUser : UserModel[];

  selectedTask : ParentTaskModel;
  selectedProject : ProjectModel;
  selectedUser : UserModel;


  searchForm = new FormGroup({
  })

  txtSearchTarget : string;
  constructor(public activeModal: NgbActiveModal, private userService : UserService, private taskService : TaskService, private projectService: ProjectService) { }

  ngOnInit() {
    this.loadData();
  }

  loadData(){
   if(this.searchType == "UserData"){
      this.getAllUsers();
    }
    else if(this.searchType =="ProjectData"){
      this.getAllProjects();
    }
    else if(this.searchType =="ParentTaskData"){
      this.getAllTasks();
    }
  }

  getAllUsers(){
    this.userService.GetAllUserDetails().subscribe(data =>{this.allUser = data});
 }

  getAllTasks(){
  this.taskService.GetAllParentTasks().subscribe(data =>{ 
    this.allTask = data
  });
}

  getAllProjects(){
  this.projectService.GetAllProjectDetails().subscribe(data =>{this.allProject = data});
}

  closeModal() {
    this.activeModal.close('Modal Closed');
  }

  public submitForm() {
    if(this.searchType == "UserData"){
      this.searchResult.emit(this.selectedUser);
      this.activeModal.close(this.selectedUser);
     }
     else if(this.searchType =="ProjectData"){
       this.searchResult.emit(this.selectedProject);
       this.activeModal.close(this.selectedProject);
     }
     else if(this.searchType =="ParentTaskData"){
      this.searchResult.emit(this.selectedTask);
      this.activeModal.close(this.selectedTask);
     }
     else{
      this.activeModal.close();
     }
  }

  public SelectData(event: any, selectedData : any){
    if(event.target.checked){
      if(this.searchType == "UserData"){
       this.selectedUser = selectedData;
      }
      else if(this.searchType =="ProjectData"){
        this.selectedProject = selectedData
      }
      else if(this.searchType =="ParentTaskData"){
       this.selectedTask = selectedData;
      }
    }
  }
}

