import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectComponent } from './Views/project/project.component';
import { UserComponent } from './Views/user/user.component';
import { AddTaskComponent } from './Views/task/addtask/addtask.component';
import { ViewtaskComponent } from './Views/task/viewtask/viewtask.component';
 

import { ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule } from '@angular/common/http';
 
import {FormsModule} from  '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
 
import { Injectable } from '@angular/core';
import { TaskService } from 'src/app/Services/Task/task.service';
import { UserService } from 'src/app/Services/User/user.service';
import { ProjectService } from 'src/app/Services/Project/project.service';
import { PopupModelComponent } from './popup-model/popup-model/popup-model.component';

import { SortingpipeComponent } from './Sorting/sortingpipe.component';

import { SearchpipePipe } from './Filters/searchpipe.pipe';
import { DatePipe } from '@angular/common';
 

 
const appRoutes: Routes = [
  { path: 'AddProject',        component: ProjectComponent },
  { path: 'AddTask', component: AddTaskComponent },
  { path: 'AddUser',        component: UserComponent }, 
  { path: 'ViewTask',        component: ViewtaskComponent },

  {path : '', component : AddTaskComponent},
  ];



@NgModule({
  declarations: [
    AppComponent,
    
    ProjectComponent,
    UserComponent,     
    ViewtaskComponent,
    AddTaskComponent,
    
    PopupModelComponent,
    SortingpipeComponent,
    SearchpipePipe,
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule
    ,NgbModule
    ,ReactiveFormsModule
    ,HttpClientModule
    ,FormsModule
    ,RouterModule.forRoot(appRoutes, {enableTracing : true})
  ],
  providers: [TaskService,UserService,ProjectService,DatePipe],
  bootstrap: [AppComponent],
  entryComponents : [PopupModelComponent]
})
export class AppModule { }
