import { UserModel } from 'src/app/Models/UserModel';
export class ProjectModel { 
    public PROJECT_ID:number;
    public PROJECT:string;    
    public START_DATE :string;
    public END_DATE :string;      
    public PRIORITY :number;   
}

export class ProjectTaskDetails{
    public  Project_ID : string
    public  Project_Name : string
    public  PROJECT :string
    public  Start_Date : string
    public  End_Date : string
    public  Priority : number
    public  NoOfTasks : number
    public  NoOfTasksCompleted : number
    public  Manager : UserModel
}



