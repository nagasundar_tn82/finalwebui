import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 
import { AddTaskComponent } from 'src/app/Views/task/addtask/addtask.component'


import { ViewtaskComponent } from 'src/app/Views/task/viewtask/viewtask.component';
 
import { ProjectComponent } from 'src/app/Views/project/project.component';

import { UserComponent } from 'src/app/Views/user/user.component';

const routes: Routes = [  
  { path: '',   redirectTo: '/ProjectTask', pathMatch: 'full' },
{ path: 'ProjectTask',        component: ProjectComponent },
// { path: 'AddTask', component: AddTaskComponent },
{path:'AddTask/:TASK_ID', component:AddTaskComponent},
{ path: 'UserTask',        component: UserComponent },
{ path: 'ViewTask',        component: ViewtaskComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }