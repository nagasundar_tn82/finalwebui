import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectComponent } from './project.component'; 
 

import { BrowserModule} from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';

import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
 




import { Component, OnInit } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormGroup,FormControl, Validators } from '@angular/forms';
// import  NgbModal from 'otstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import {ProjectService} from  'src/app/Services/Project/project.service';
import {UserService} from  'src/app/Services/User/user.service';


import { ProjectModel,ProjectTaskDetails } from 'src/app/Models/ProjectModel';

 
import { PopupModelComponent } from 'src/app/popup-model/popup-model/popup-model.component';
import { Console } from '@angular/core/src/console';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
//import { SearchpipePipe } from 'src/app/pipe/searchpipe'

 
import {Observable} from 'rxjs';
import { Data } from '@angular/router/src/config';
import { NgModule } from '@angular/core';
import { UserModel } from 'src/app/models/UserModel';
import { providerDef } from '@angular/core/src/view/provider';
import { SortingpipeComponent } from 'src/app/Sorting/sortingpipe.component';

import { SearchpipePipe } from 'src/app/Filters/searchpipe.pipe';


describe('ProjectComponent', () => {
  let component: ProjectComponent;
  let fixture: ComponentFixture<ProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, RouterTestingModule,        
                HttpClientModule,
                FormsModule,
                RouterModule,
                ReactiveFormsModule       
              ],
      declarations: [ ProjectComponent,SortingpipeComponent,SearchpipePipe ],
      providers: [UserService,ProjectService]
    })
    .compileComponents();
  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(ProjectComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });



    beforeEach(() => {
    fixture = TestBed.createComponent(ProjectComponent);
    component = fixture.componentInstance;
    let projService:ProjectService;  
    let usrService :UserService;    
    //let getValue:any={};
    //getValue.innerHTML = 'Test';
    //spyOn(component, 'AddProjectDetails').and.returnValue(getValue);    
    fixture.detectChanges();
  });

  it('should create', () => {    
    expect(component).toBeTruthy();
  });

   it('GetAllProjects', () => {    
  //   //expect(component).toBeTruthy();
     component.GetAllProjectDetails();
    
   });

});



 

// describe('ViewtaskComponent', () => {
//   let component: ViewtaskComponent;
//   let fixture: ComponentFixture<ViewtaskComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [BrowserModule, RouterTestingModule,        
//         HttpClientModule,
//         FormsModule,
//         RouterModule        
//       ],
//       declarations: [ ViewtaskComponent,AppComponent,FilterPipe ],
//       providers: [TaskserviceService],
//     })
//     .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(ViewtaskComponent);
//     component = fixture.componentInstance;
//     let taskserviceService:TaskserviceService;      
//     let getValue:any={};
//     getValue.innerHTML = 'Test';
//     spyOn(component, 'setaddTask').and.returnValue(getValue);
//     spyOn(component, 'setviewTask').and.returnValue(getValue);
//     fixture.detectChanges();
//   });

//   it('should create', () => {    
//     expect(component).toBeTruthy();
//   });

//   it('getAllTaskse', () => {    
//     //expect(component).toBeTruthy();
//     component.getAllTasks();
    
//   });

  
// });
