import { Component, OnInit } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormGroup,FormControl, Validators } from '@angular/forms';
// import  NgbModal from 'otstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import {ProjectService} from  'src/app/Services/Project/project.service';
import {UserService} from  'src/app/Services/User/user.service';


import { ProjectModel,ProjectTaskDetails } from 'src/app/Models/ProjectModel';

 
import { PopupModelComponent } from 'src/app/popup-model/popup-model/popup-model.component';
import { Console } from '@angular/core/src/console';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
//import { SearchpipePipe } from 'src/app/pipe/searchpipe'

 
import {Observable} from 'rxjs';
import { Data } from '@angular/router/src/config';
import { NgModule } from '@angular/core';
import { UserModel } from 'src/app/Models/UserModel';
 
 
 

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css'],
  providers :[DatePipe]
})
export class ProjectComponent implements OnInit {
  projectForm = new FormGroup({
    Project_Name : new FormControl(),    
    Project_ID : new FormControl(), 
    // Project_Name : new FormControl('',Validators.required),
    PROJECT : new FormControl(),
    Start_Date : new FormControl(),
    End_Date : new FormControl(),
    Priority : new FormControl(),
     NoOfTasks : new FormControl(),
     NoOfTasksCompleted : new FormControl(),
     Manager : new FormControl()
  })


  newProject = new FormGroup({
    PROJECT_ID : new FormControl(),    
    PROJECT : new FormControl(), 
    // Project_Name : new FormControl('',Validators.required),
    START_DATE : new FormControl(),
    END_DATE : new FormControl(),
    PRIORITY : new FormControl(),
    //  NoOfTasks : new FormControl(),
    //  NoOfTasksCompleted : new FormControl(),
    //  Manager : new FormControl()
  })

  // userDetail = new FormGroup({
  //    USER_ID:new FormControl(),
  //    FIRST_NAME:new FormControl(),
  //    LAST_NAME :new FormControl(),
  //    EMPLOYEE_ID :new FormControl(),
  //    PROJECT_ID :new FormControl()

  // })


  //newProject :ProjectModel;
  //  d: Date = new  Date();
sortingName: string;
isDesc: boolean;
selectedManager : UserModel;
 // public txtSearchProject = new FormControl('');
  showMsg: string = null;
  public selectedManagerName : string = "";
  public selectedManager1 = new UserModel;
   
  constructor(private serviceVariable:ProjectService, private userService : UserService, private popup : NgbModal,private myDate : DatePipe) {}
  public projectCollection : ProjectModel[];
  allProjectsTasksCollection : ProjectTaskDetails[] = [];
  //public myDate : DatePipe;
  //le:ProjectService, public projectCollection : ProjectModel[];
  //public newProjectDetails : ProjectModel;


  ngOnInit() {
    //$datePicker.datepicker( optComponent );
    this.GetAllProjectDetails();this.GetAllProjectTaskDetails();
   
  }

  isDateEnable :boolean;
  isDatesEnabled(event:any){
    this.isDateEnable = event.target.checked;
  //let Date = new DatePipe();
  let mygetDate = new Date();
  this.projectForm.value.START_DATE = this.myDate.transform(mygetDate, 'yyyy-MM-dd');
  this.projectForm.value.END_DATE = this.myDate.transform(this.AddOneMoreDay(mygetDate,1), 'yyyy-MM-dd');

    // //$scope.date = new Date();
    //   //this.projectForm.patchValue.transform(this.d, 'yyyy-MM-dd');
    //   this.projectForm.patchValue({
    //     START_DATE: Date.now(),
    //     END_DATE : Date.now()
    //  });

    
  }

  // GetAllProjects()
  // {

  // }
  GetAllProjectTaskDetails()
  {
        this.serviceVariable.GetAllProjectTaskDetails().subscribe(data =>
      {
        this.allProjectsTasksCollection = data
        //debugger;

        // this.serviceVariable.GetAllProjectDetails(data1 =>
        // {
        //     this.allProjectsTasksCollection.push(data1.PROJECT); 
        
        // } );



        // this.serviceVariable.GetAllProjectDetails().subscribe(data1 =>
        // {
        //     this.allProjectsTasksCollection.push(data1.PROJECT); 
        
        // } );

      });
  }



//   getAllProjectDetails(){
//     this.projectService.getProjectDetails().subscribe(data =>{this.allProjectDetails = data});
//  }
//   getAllProjects(){
//     this.serviceVariable.getProjects().subscribe(data =>
//       {
//         this.allProjects = data
//       });
//  }
  AddProjectDetails()
  {       
   //this.newProject = this.projectForm.value; 
    //debugger;
    //this.newProject.PROJECT_ID = this.projectForm.value.Project_ID;

    this.newProject.value.PROJECT_ID =  this.projectForm.value.Project_ID;

    this.newProject.value.PROJECT =  this.projectForm.value.Project_Name;

    this.newProject.value.START_DATE =  this.projectForm.value.Start_Date;
    this.newProject.value.END_DATE =  this.projectForm.value.End_Date;

    this.newProject.value.PRIORITY =  this.projectForm.value.Priority;
 
     var tes = this.projectForm.value.Manager;


    // this.newProject.PROJECT = this.projectForm.value.Project_Name;    
    // this.newProject.START_DATE = this.projectForm.value.Start_Date;
    // this.newProject.END_DATE = this.projectForm.value.End_Date;
    // this.newProject.PRIORITY = this.projectForm.value.Priority;

    if(this.projectForm.valid){
      debugger;
     // this.newProject = this.projectForm.value;
      if(!this.newProject.value.PROJECT_ID)
      {


        this.serviceVariable.AddProjectDetails(this.newProject.value).subscribe(
          data=> 
          {
        
          if(data.PROJECT_ID != null){
            console.log('Project Added'); 
            //this.GetAllProjectDetails.push(data);
            if(this.selectedManagerName != ""){
            
              this.selectedManager.PROJECT_ID = data.PROJECT_ID;                    
              this.userService.ModifyUserDetails(this.selectedManager).subscribe(userData =>
              { 
              

              });
            }
            this.GetAllProjectDetails();
            this.GetAllProjectTaskDetails();
            this.resetProjectForm();
          }
        })
    }
      else{
        debugger;
        this.serviceVariable.UpdateProjectDetails(this.newProject.value).subscribe(data=> 
          {
            // debugger;
            // if(data.PROJECT_ID != null){
            // console.log('Project Updated'); 
            debugger;
            if(this.selectedManagerName != "")
            {
              debugger;
              this.selectedManager.PROJECT_ID = data.PROJECT_ID;   
                this.userService.ModifyUserDetails(this.selectedManager).subscribe();//              //   this.resetProjectForm());
             }
          //   else{
          //     alert("Project not updated");
          //   }
            
 
      


      
        this.GetAllProjectDetails();
        this.GetAllProjectTaskDetails();
    
    })     
     this.showMsg = "Added Successfully";    
  }

  }
  }
  AssingProjectDetails(value : ProjectTaskDetails)
  {   
    this.projectForm.setValue(value);
   // debugger;
    if (this.projectForm.value.Manager != null)
    {
      this.selectedManagerName = this.projectForm.value.Manager.FIRST_NAME;
    }
    else 
    {
      this.selectedManagerName = null;
    }
    
   // selectedManagerName
    //debugger;
    document.getElementById("btnSubmit").innerText = "Update Project";
   // this.isUpdate  = true;     
  }
 
  public UpdateProject()
  {        
    this.serviceVariable.UpdateProjectDetails(this.projectForm.value).subscribe(data=> 
      {         if(this.selectedManagerName != "")
      {
         
          this.userService.ModifyUserDetails(this.selectedManager).subscribe();//              //   this.resetProjectForm());
       }
       this.GetAllProjectDetails();
       this.resetProjectForm();
       this.showMsg = "Updated Successfully";   
   });
  }

  AddOneMoreDay(date:Date , Days:number) :Date
 {
      date.setDate(date.getDate()+ Days);
      return date;
 }

 public GetAllProjectDetails()
 {        
   this.serviceVariable.GetAllProjectDetails().subscribe(data =>{this.projectCollection = data

   
  }
);
 }
 sort(name: string): void {
  if (name && this.sortingName !== name) {
    this.isDesc = false;
  } else {
    this.isDesc = !this.isDesc;
  }
  this.sortingName = name;
}


resetProjectForm(){
  this.projectForm.reset();      
    this.selectedManagerName = "";  
    this.selectedManager = null;
    document.getElementById("btnSubmit").innerText = "Add Project";
  }

searchUserManagerData(){ 
  const searchPopup = this.popup.open(PopupModelComponent);
  searchPopup.componentInstance.searchType = 'UserData';
  searchPopup.componentInstance.searchResult.subscribe((data) =>
 {
   //  alert(data.FIRST_NAME);
     this.selectedManagerName = data.FIRST_NAME;
     this.projectForm.patchValue({USER_ID : data.USER_ID});
     this.selectedManager = data;
     
 });
}

suspendProject(value: number)
{  
  this.serviceVariable.DeleteProjectDetails(value).subscribe((RetVal:boolean)=> {if (RetVal == true)  
   {           
     //let userIDIndex =  this.projectCollection.findIndex(a=>a.PROJECT_ID == value);
     //this.projectCollection.splice(userIDIndex,1);
     this.GetAllProjectDetails();
     this.GetAllProjectTaskDetails();
     //alert("User deleted Successfully");     
 } else { this.showMsg = "User cannot be deleted;" }  });   
  //this.showMsg = "Updated Successfully";
  
}


 


}
