import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewtaskComponent } from './viewtask.component';
import { BrowserModule} from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';

import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SortingpipeComponent } from 'src/app/Sorting/sortingpipe.component';

import { SearchpipePipe } from 'src/app/Filters/searchpipe.pipe';

import { Component, OnInit } from '@angular/core';

import {FormsModule,ReactiveFormsModule} from '@angular/forms';

import { FormGroup,FormControl } from '@angular/forms';

//import { FormGroup,FormControl, Validators } from '@angular/forms';

import { TaskModel } from 'src/app/Models/TaskModel';

import { Console } from '@angular/core/src/console';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PopupModelComponent } from 'src/app/popup-model/popup-model/popup-model.component';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import {ActivatedRoute} from '@angular/router';
import {ProjectService} from  'src/app/Services/Project/project.service';
import {TaskService} from  'src/app/Services/Task/task.service';


import {Observable} from 'rxjs';
import { Data } from '@angular/router/src/config';

import { NgModule } from '@angular/core';
//import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


describe('ViewtaskComponent', () => {
  let component: ViewtaskComponent;
  let fixture: ComponentFixture<ViewtaskComponent>;

  // beforeEach(async(() => {
  //   TestBed.configureTestingModule({
  //     declarations: [ ViewtaskComponent ]
  //   })
  //   .compileComponents();
  // }));



  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, RouterTestingModule,        
                HttpClientModule,               
                FormsModule,
                RouterModule,
                ReactiveFormsModule                       
              ],
      declarations: [ViewtaskComponent,SortingpipeComponent,SearchpipePipe],
      providers: [ProjectService,TaskService]      
    })
    .compileComponents();
  }));



  beforeEach(() => {
    fixture = TestBed.createComponent(ViewtaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('GetAllTaskDetails', () => {    
    //   //expect(component).toBeTruthy();
       component.GetAllTaskDetails();
      
     });
});
