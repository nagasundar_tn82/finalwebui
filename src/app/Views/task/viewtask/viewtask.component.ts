import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl } from '@angular/forms';
import {Observable} from 'rxjs';
import { Data } from '@angular/router/src/config';
import { NgModule } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';
import { TaskService } from 'src/app/Services/Task/task.service';
import { TaskModel } from 'src/app/Models/TaskModel';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PopupModelComponent } from 'src/app/popup-model/popup-model/popup-model.component';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';



@Component({
  selector: 'app-viewtask',
  templateUrl: './viewtask.component.html',
  styleUrls: ['./viewtask.component.css']
})
export class ViewtaskComponent implements OnInit {

  public selectedProjectName : string = "";

  // public txtParentTask = new FormControl('');
  // public txtPrioriyFrom = new FormControl('');
  // public txtPrioriyTo = new FormControl('');
  // public dtStartDate = new FormControl('');
  // public dtEndDate = new FormControl('');

 
  frmViewTaskGroup = new FormGroup({
    TASK_ID : new FormControl(''),
    TASK :  new FormControl(''),
    PRIORITY :new FormControl(''),
    PARENT_ID : new FormControl(''),
    START_DATE :new FormControl(''),
    END_DATE : new FormControl(''),
    STATUS : new FormControl(''),
    PARENTNAME : new FormControl('')
  });  


  public eachTask : TaskModel[];
   sortingName: string;
  isDesc: boolean;
  constructor(private serviceVariable:TaskService,private popup : NgbModal) {}
  public taskCollection : any[] = [];
  public parentTaskCollection  : any[];
  public finalCollection  : any[]= [];

  ngOnInit() {
     
    this.GetAllTaskDetails();
  }

  public GetAllTaskDetails()
  {     
    //this.serviceVariable.GetAllTaskDetails().subscribe((TaskIDVar:TaskModel[])=> this.taskCollection=TaskIDVar); 

    this.serviceVariable.GetAllParentTasks().subscribe(data1 => 
      { 
          
        this.parentTaskCollection = data1
       
          this.serviceVariable.GetAllTaskDetails().subscribe(data=>  
            {
            this.finalCollection= data
           
    this.finalCollection.forEach(element => { 
     
      //if (this.parentTaskCollection.filter(a=> a.PARENT_ID[0]) == element.PARENT_ID)
      //{
        
         if( this.parentTaskCollection.some(a=> a.PARENT_ID==  element.PARENT_ID))
         {
        element.PARENTNAME = this.parentTaskCollection.filter(a=> a.PARENT_ID==  element.PARENT_ID)[0].PARENT_TASK
         }
      //}   
      this.taskCollection.push(element)

    })
    
    })
      
    });
          

      }
    
    
 
    
      
      
      
     
      //  this.serviceVariable.GetParentTask(data.PARENT_ID).subscribe()
      
      
      
 

  EndTask(SelectedTask : TaskModel)
  {
     let index =  this.taskCollection.findIndex(a=>a.TASK_ID == SelectedTask.TASK_ID);
     let TaskToBeEnded = this.taskCollection[index];
     TaskToBeEnded.STATUS = 1;
     //this.serviceVariable.ModifyTaskDetails(this.frmViewTaskGroup.value).subscribe();

      

     this.serviceVariable.ModifyTaskDetails(TaskToBeEnded).subscribe(data=> {    this.GetAllTaskDetails(); });
 

    // let index =  this.taskCollection.findIndex(a=>a.TaskID == Task_ID);
    // let TaskToUpdate = this.taskCollection[index];
    // TaskToUpdate.IsTaskEnd = true;

    // let TaskToUpdate = this.taskCollection[index];
    // //TaskToUpdate.IsTaskEnd = true;
    // this.serviceVariable.ModifyTaskDetails(TaskToUpdate).subscribe();   
   //this.showMsg = "Updated Successfully";



  //  deleteTask(id:string){
  //   this.taskService.deleteTask(id).subscribe(data=> {
      
  //    if(data){
  //      this.allTask.splice(this.allTask.findIndex(a=>a.Task_ID == id),1);
  //    }
  //    else{
  //      alert("Task is not deleted");
  //    }
  //    });
  //  }


   }

  

  searchProjectData(){
    const searchPopup = this.popup.open(PopupModelComponent);
    searchPopup.componentInstance.searchType = 'ProjectData';
    searchPopup.componentInstance.searchResult.subscribe((data) =>
   {
     //  alert(data.TASK);
       this.selectedProjectName = data.PROJECT;   
       this.frmViewTaskGroup.patchValue({Parent_ID : data.PROJECT});    
   });
 
   }


 


  sort(name: string): void {
    if (name && this.sortingName !== name) {
      this.isDesc = false;
    } else {
      this.isDesc = !this.isDesc;
    }
    this.sortingName = name;
  }

}
