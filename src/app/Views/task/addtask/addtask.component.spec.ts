import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// import { AddTaskComponent } from 'src/app/Views/task/addtask/task.component'; 
// './task/addtask/project.component'; 

import { AddTaskComponent } from './addtask.component'; 
// import { AddTaskComponent } from './task/addtask/addtask.component'; 

import { BrowserModule} from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';

import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';


//import { async, ComponentFixture, TestBed } from '@angular/core/testing';

//import { AddtaskComponent } from './addtask.component';

import { SortingpipeComponent } from 'src/app/Sorting/sortingpipe.component';

import { SearchpipePipe } from 'src/app/Filters/searchpipe.pipe';

import { Component, OnInit } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormGroup,FormControl, Validators } from '@angular/forms';
 
// import { TaskService } from 'src/app/Services/Task/task.service';
import { TaskModel } from 'src/app/Models/TaskModel';
 
import { Console } from '@angular/core/src/console';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PopupModelComponent } from 'src/app/popup-model/popup-model/popup-model.component';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import {ActivatedRoute} from '@angular/router';
import {ProjectService} from  'src/app/Services/Project/project.service';
import {UserService} from  'src/app/Services/User/user.service';
import {TaskService} from  'src/app/Services/Task/task.service';





describe('AddTaskComponent', () => {
  //let component: AddTaskComponent;

  let component: AddTaskComponent;
  let fixture: ComponentFixture<AddTaskComponent>;
  
  //fixture = TestBed.createComponent(AddTaskComponent);
  //let fixture: ComponentFixture<AddTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, RouterTestingModule,        
                HttpClientModule,
                FormsModule,
                RouterModule,
                ReactiveFormsModule                       
              ],
      declarations: [ AddTaskComponent,SortingpipeComponent,SearchpipePipe],
      providers: [UserService,ProjectService,TaskService]      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {      
    expect(component).toBeTruthy();
  });

    it('loadTask', () => {    
  //   //expect(component).toBeTruthy();
     component.loadTask();
    
   });

});




// describe('AddtaskComponent', () => {
//   let component: AddtaskComponent;
//   let fixture: ComponentFixture<AddtaskComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [ AddtaskComponent ]
//     })
//     .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(AddtaskComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//});
