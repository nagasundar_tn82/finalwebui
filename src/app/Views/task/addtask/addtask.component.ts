import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/Services/Task/task.service';
import { TaskModel } from 'src/app/Models/TaskModel'
import { FormGroup,FormControl } from '@angular/forms';
import { Console } from '@angular/core/src/console';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PopupModelComponent } from 'src/app/popup-model/popup-model/popup-model.component';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import {ActivatedRoute} from '@angular/router';
import {ProjectService} from  'src/app/Services/Project/project.service';
import {UserService} from  'src/app/Services/User/user.service';
import { UserModel } from 'src/app/Models/UserModel';

import { ProjectModel,ProjectTaskDetails } from 'src/app/Models/ProjectModel';
import { ParentTaskModel } from '../../../Models/ParentTaskModel';


@Component({
  selector: 'app-task',
  templateUrl: './addtask.component.html',
  styleUrls: ['./addtask.component.css']
})
export class AddTaskComponent implements OnInit {

  frmTaskGroup = new FormGroup({
    TASK_ID : new FormControl(''),
    TASK :  new FormControl(''),
    PRIORITY :new FormControl(''),
    PARENT_ID : new FormControl(''),
    START_DATE :new FormControl(''),
    PROJECT_ID  :new FormControl(''),
    STATUS  :new FormControl(''),
    END_DATE : new FormControl(''),
    USER_ID : new FormControl(''),
    PARENT_NAME : new FormControl('')

  });  

    public searchTask : string;
    public taskCollection : TaskModel[];    
    public parentTask = new ParentTaskModel;
    showMsg: string = null;

    isParentTask : boolean;
    public selectedUserName : string = "";
    public selectedTaskName : string = "";
    public selectedParentName : string = "";
    public selectedProjectName : string = "";
    
    public selectedUserID : number;    
    public selectedParentID : number;
    public selectedProjectID : number;

    managerModel : UserModel;
    projectModel : ProjectModel;
    public parentID : number;
    public parentName : string="";

    isCheckedEdit : number;
    public IsComingFromViewTask :boolean = false;


  constructor(private taskService:TaskService, private route : ActivatedRoute,private popup : NgbModal,private projectService:ProjectService, private userService : UserService) 
  { }

  ngOnInit() {   
    this.IsComingFromViewTask = false;
    this.loadTask(); 
     
  }

  loadTask(){
 
    if(this.route.snapshot.paramMap.get('TASK_ID')){    
      this.IsComingFromViewTask = true;
     this.taskService.getTaskName(this.route.snapshot.paramMap.get('TASK_ID')).subscribe(
      
       data=>
       {
      
         // this.frmTaskGroup.setValue(data)    
         this.frmTaskGroup.patchValue({PARENT_ID : data.PARENT_ID});
         this.frmTaskGroup.patchValue({TASK_ID : data.TASK_ID});
         this.frmTaskGroup.patchValue({PROJECT_ID : data.PROJECT_ID});
         this.frmTaskGroup.patchValue({TASK : data.TASK});
         this.frmTaskGroup.patchValue({START_DATE : data.START_DATE});
         this.frmTaskGroup.patchValue({END_DATE : data.END_DATE});
         this.frmTaskGroup.patchValue({PRIORITY : data.PRIORITY});
         this.frmTaskGroup.patchValue({STATUS : data.STATUS});
           
           
        //  if (data.PARENT_ID > 0)
        //  {
           
        //   this.isCheckedEdit = 1; 
        //   this.isParentTask = true;
         
        //  }
        //  else 
        //  {
        //  // this.isParentTaskChecked(false);
        //  this.isCheckedEdit = 0;
        //  this.isParentTask = false;
        //  }
        
        this.isParentTask = false;
        this.isCheckedEdit = 0;
           
        //this.isCheckedEdit
           
        //  USER_ID  
        //  PARENT_NAME  

       if(data){
       // this.frmTaskGroup.setValue(data);
        if(data.PROJECT_ID){
          this.projectService.GetSearchProjectID(data.PROJECT_ID).subscribe(proData => {
           // debugger;
              if(proData){
               // debugger;
                this.selectedProjectName = proData.PROJECT;
              }
          });
        }

        this.userService.GetSearchUserIDByTaskID(data.TASK_ID).subscribe(usrData => {
       // debugger;
             if(usrData){
              // debugger;
               this.selectedUserName = usrData.FIRST_NAME;
             }
         });

         this.taskService.GetParentName(data.PARENT_ID).subscribe(parentData => {
         
          if(parentData){
         
            this.selectedParentName = parentData.PARENT_TASK;
          }
      });
       
        
        document.getElementById("btnSubmit").innerText = "Update Task";
      }


       }
      );
     
     // this.frmTaskGroup.patchValue({StartDate : "26/05/2019"});  
     
     ////workign finethis.taskService.getTaskName(this.route.snapshot.paramMap.get('TaskID')).subscribe((TaskIDVar:Task)=> this.frmTaskGroup.patchValue({TaskName : TaskIDVar.TaskName}));    
     //this.taskService.getTaskName(this.route.snapshot.paramMap.get('TASK_ID')).subscribe((TaskIDVar:TaskModel)=> this.frmTaskGroup.patchValue({TaskName : TaskIDVar.TASK}));    
     //this.taskService.getTask().subscribe((TaskIDVar:Task)=> this.frmTaskGroup.setValue(TaskIDVar));
    // this.taskService.getTaskName(value.value).subscribe((TaskIDVar:Task)=> this.frmTaskGroup.setValue(data));
    }
    else{
      this.IsComingFromViewTask = false;
    }
  }

  showTask() {
    this.taskService.getTask().subscribe((TaskIDVar:TaskModel)=> this.frmTaskGroup.setValue(TaskIDVar));
   }
   showTaskName(value:any) {         
    //this.taskService.getTaskName(value.value).subscribe((TaskIDVar:TaskModel)=> this.frmTaskGroup.patchValue({TaskName : TaskIDVar.TaskName}));     
   }
   AddTaskDetails()
   {       
   // this.frmTaskGroup.value.PARENT_ID = 0;

    
   if (document.getElementById("btnSubmit").innerText == "Update Task")
   {
      this.taskService.ModifyTaskDetails(this.frmTaskGroup.value).subscribe(data=> {            
        {
         this.managerModel.TASK_ID = data.TASK_ID; 
          
        this.userService.UpdateUserOnEditTask(this.managerModel).subscribe();
        }

        console.log('Success'); this.GetAllTaskDetails(); //});     
        this.showMsg = "Task Updated  Successfully";   
  
     });               
       
 

        
   }

   else 
   {
    if (this.isParentTask)
    {

   
      
      //this.parentTask.PARENT_ID = this.frmTaskGroup.value.PARENT_ID;
      this.parentTask.PARENT_TASK = this.frmTaskGroup.value.TASK;
      //this.parentTask.PARENT_ID = this.frmTaskGroup.value.PARENT_ID;
      //this.taskService.AddParentTask(this.parentTask).toPromise().then(alert("hi"));
       this.taskService.AddParentTask(this.parentTask).subscribe();

//         //         this.taskService.GetParentID(this.parentTask.PARENT_TASK).subscribe( 
//         //   data1=> {  
//         //     this.frmTaskGroup.value.PARENT_ID = data1.PARENT_ID;
//         //     this.frmTaskGroup.patchValue({PARENT_ID : data1.PARENT_ID});
//         //   }
//         // );
// debugger;
// console.log( data.PARENT_ID);
//             this.frmTaskGroup.value.PARENT_ID = data.PARENT_ID;
//             this.frmTaskGroup.patchValue({PARENT_ID : data.PARENT_ID});
//       }

      

    }
  else
  {
     this.taskService.AddTaskDetails(this.frmTaskGroup.value).subscribe(data=> {      
      // if (!(this.isParentTask))
      {

       this.managerModel.TASK_ID = data.TASK_ID; 
        
      this.userService.ModifyUserDetails(this.managerModel).subscribe();
      }

   });
  }

 //debugger;
 //this.frmTaskGroup.value.PARENT_ID =2

 
        console.log('Success'); this.GetAllTaskDetails(); //});     
      this.showMsg = "Added Successfully";    

   // }

   }



}




   UpdateTaskDetails()
   {       
     this.taskService.ModifyTaskDetails(this.frmTaskGroup.value).subscribe( (data=> {this.GetAllTaskDetails();})    )     ;
   }

   DeleteTaskDetails(value:any)
   {   
    this.taskService.DeleteTaskDetails(value.value).subscribe( (data=> {this.GetAllTaskDetails();})    )     ;
   }

   GetAllTaskDetails()
   {
     this.taskService.GetAllTaskDetails().subscribe((TaskIDVar:TaskModel[])=> this.taskCollection=TaskIDVar);
   }
   resetTaskForm(){
    this.frmTaskGroup.reset();      
      this.selectedUserName = "";
      this.selectedTaskName = "";
      this.selectedParentName = "";
      this.selectedProjectName = "";
      document.getElementById("btnSubmit").innerText = "Add Task";
      // document.getElementById("userName").value = "";
      // document.getElementById("projName").value = "";
      
    }
  
    searchParentTask(){
   const searchPopup = this.popup.open(PopupModelComponent);
   searchPopup.componentInstance.searchType = 'ParentTaskData';
   searchPopup.componentInstance.searchResult.subscribe((data) =>
  {
     // alert(data.TASK);
      this.selectedParentName = data.PARENT_TASK;
      //this.frmTaskGroup.patchValue({TASK_ID : data.TASK_ID});
      this.frmTaskGroup.patchValue({PARENT_ID : data.PARENT_ID});
      this.frmTaskGroup.patchValue({PARENT_NAME : data.PARENT_TASK});
      

      this.parentID = data.TASK_ID;
      this.parentName = data.TASK;


  });

  }


  searchUserData(){
    const searchPopup = this.popup.open(PopupModelComponent);
    searchPopup.componentInstance.searchType = 'UserData';
    searchPopup.componentInstance.searchResult.subscribe((data) =>
   {
      // alert(data.FIRST_NAME);
       this.selectedUserName = data.FIRST_NAME;
       this.frmTaskGroup.patchValue({USER_ID : data.USER_ID});
       this.managerModel = data;

       
   });
 
   }

   searchProjectData(){
    const searchPopup = this.popup.open(PopupModelComponent);
    searchPopup.componentInstance.searchType = 'ProjectData';
    searchPopup.componentInstance.searchResult.subscribe((data) =>
   {
     //  alert(data.TASK);
       
        
    //   this.frmTaskGroup.patchValue({Project_Id = data.});


    this.selectedProjectName = data.PROJECT;
    this.frmTaskGroup.patchValue({PROJECT_ID : data.PROJECT_ID});   
    this.projectModel = data;

   });
 
  }
  isParentTaskChecked(event:any){
    this.isParentTask = ((event.target.checked)) ;
    
}


  }
