import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/Services/User/user.service';
import { UserModel } from 'src/app/Models/UserModel';

import { FormGroup,FormControl } from '@angular/forms';
import {Observable} from 'rxjs';
import { Data } from '@angular/router/src/config';
import { NgModule } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {Validators} from '@angular/forms';
 
import { Console } from '@angular/core/src/console';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit { 

  public eachTask : UserModel[];
  sortingName: string;
  isDesc: boolean;

  constructor(private serviceVariable:UserService) {}
  public userCollection : UserModel[];
  showMsg: string = null;
   isUpdate :boolean = false;
   modifiedUserID : UserModel;
  
  frmUserGroup = new FormGroup({   
    USER_ID : new FormControl(''),
    FIRST_NAME : new FormControl('', Validators.required),
    LAST_NAME : new FormControl(''),
    EMPLOYEE_ID : new FormControl('', Validators.required),
    PROJECT_ID : new FormControl('', Validators.required),
    TASK_ID : new FormControl('')   
  });  



  ngOnInit() {
    this.GetAllUserDetails()
  }

  showUser() {
    //this.serviceVariable.getUser().subscribe((UserIDVar:UserModel)=> this.frmUserGroup.setValue(UserIDVar));
   }
  //  showUserName(value:any) {         
  //   //this.serviceVariable.getUserName(value.value).subscribe((UserIDVar:UserModel)=> this.frmUserGroup.patchValue({UserName : UserIDVar.UserName}));     
  //  }
   AddUserDetails()
   {       
     if (this.isUpdate == false)
     {
     this.serviceVariable.AddUserDetails(this.frmUserGroup.value).subscribe(data=> {    this.GetAllUserDetails(); });               
     
     this.showMsg = "User Added Successfully";   
     }
     else
     {
      this.UpdateUserDetails();
     }
     this.frmUserGroup.reset();

   }

  //  UpdateUserDetails()
  //  {  
  //    this.serviceVariable.ModifyUserDetails(this.frmUserGroup.value).subscribe();
  //  }

  //  DeleteUserDetails(value:any)
  //  {   
  //   this.serviceVariable.DeleteUserDetails(value.value).subscribe();
  //  }

   GetAllUserDetails()
   {
     this.serviceVariable.GetAllUserDetails().subscribe((UserIDVar:UserModel[])=> this.userCollection=UserIDVar);
   }
  AssignUserDetails(value : UserModel)
  {   
    this.frmUserGroup.setValue(value);
    document.getElementById("btnSubmit").innerText = "Update User";
    this.isUpdate  = true;  
  }

   UpdateUserDetails()
   {  
     
     this.serviceVariable.ModifyUserDetails(this.frmUserGroup.value).subscribe(data=>   
     //this.showMsg = "Updated Successfully";
     {
   //  this.modifiedUserID == this.frmUserGroup.value.USER_ID;
      //debugger;
     //this.userCollection.splice(this.userCollection.findIndex(a=>a.USER_ID == this.modifiedUserID),1);
     let userIDIndex = this.userCollection.findIndex(a=>a.USER_ID ==  data.USER_ID);   
     //debugger;
     this.userCollection.splice(userIDIndex,1);     
     this.userCollection.push(data);
     this.frmUserGroup.reset();
     this.GetAllUserDetails();
     //this.frmUserGroup.setValue(this.userCollection);

     document.getElementById("btnSubmit").innerText = "Add User";
   });
     
   }
   DeleteUserDetails(value: number)
   {  
     this.serviceVariable.DeleteUserDetails(value).subscribe((RetVal:boolean)=> {if (RetVal == true)  
      {           
        let userIDIndex =  this.userCollection.findIndex(a=>a.USER_ID == value);
        this.userCollection.splice(userIDIndex,1);
        //alert("User deleted Successfully");     
        this.GetAllUserDetails();
    } else { this.showMsg = "User cannot be deleted;" }  });   
     //this.showMsg = "Updated Successfully";
     
   }
   ResetUserDetails()
   {
       this.frmUserGroup.reset();
       document.getElementById("btnSubmit").innerText = "Add User";
   }
   sort(name: string): void {
    if (name && this.sortingName !== name) {
      this.isDesc = false;
    } else {
      this.isDesc = !this.isDesc;
    }
    this.sortingName = name;
  }
}
