import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserComponent } from './user.component';

import { BrowserModule} from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';

import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SortingpipeComponent } from 'src/app/Sorting/sortingpipe.component';

import { SearchpipePipe } from 'src/app/Filters/searchpipe.pipe';

import { Component, OnInit } from '@angular/core';

import {FormsModule,ReactiveFormsModule} from '@angular/forms';

import { FormGroup,FormControl } from '@angular/forms';


import { Data } from '@angular/router/src/config';
import { NgModule } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/Services/User/user.service';

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  // beforeEach(async(() => {
  //   TestBed.configureTestingModule({
  //     declarations: [ UserComponent ]
  //   })
  //   .compileComponents();
  // }));


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, RouterTestingModule,        
                HttpClientModule,               
                FormsModule,
                RouterModule,
                ReactiveFormsModule                       
              ],
      declarations: [UserComponent,SortingpipeComponent,SearchpipePipe],
      providers: [UserService]      
    })
    .compileComponents();
  }));



  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('GetAllUserDetails', () => {    
    //   //expect(component).toBeTruthy();
       component.GetAllUserDetails();
      
     });
});
