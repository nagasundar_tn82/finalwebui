import { Pipe, PipeTransform } from '@angular/core';

import { TaskModel } from 'src/app/Models/TaskModel';
import { ProjectModel } from 'src/app/Models/ProjectModel';
import { UserModel } from 'src/app/Models/UserModel';

import { forEach } from '@angular/router/src/utils/collection';

@Pipe({
  name: 'SearchpipePipe'
})
export class SearchpipePipe implements PipeTransform {

  // transform(value: any, args?: any): any {
  //   return null;
  // }


  transform(generalList: any, searchTextName: any, typeofCollection: any): any {


    let filteredList: any=[];


    if (generalList && generalList.length){
      if (typeofCollection == "UserData")
      return generalList.filter(item =>{
          if (searchTextName && item.FIRST_NAME.toLowerCase().indexOf(searchTextName.toLowerCase()) === -1){
              return false;
          }
          return true;
        })
        else if (typeofCollection == "ProjectData")
        return generalList.filter(item =>{
          if (searchTextName && item.PROJECT.toLowerCase().indexOf(searchTextName.toLowerCase()) === -1){
              return false;
          }
          return true;
        })
        else if (typeofCollection == "ParentTaskData")
        return generalList.filter(item =>{
          if (searchTextName && item.PARENT_TASK.toLowerCase().indexOf(searchTextName.toLowerCase()) === -1){
              return false;
          }
          return true;
        })                 
  }
  else{
    return generalList;
}
  }

}
